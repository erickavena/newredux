import './App.css';
import TestComponent from "./components/Test"
function App() {
  return (
    <div className="App">
      <header className="App-header">
        <TestComponent />
      </header>
    </div>
  );
}

export default App;
