import {useEffect} from "react";
import { useDispatch, useSelector } from "react-redux";
import { TestSS } from "../store/selectors/StateSelectors";
import { TestAS } from "../store/selectors/ActionSelectors";
const Test = () => {
const dispatch = useDispatch();
const { users } = TestSS(useSelector);
const { getTestUsers } = TestAS(dispatch);

useEffect(() => {
    getTestUsers();
}, [])
return(
        <div>
            <h1>Usuarios de prueba</h1>
            <ul>
                { users.map(i => { return <li>{i.name}</li> }) }
            </ul>
        </div>
    )
}

export default Test