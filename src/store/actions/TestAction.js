import { test } from "../../constanst/ActionTypes";

const TestAction = {
  getTestUsers: () => {
    return async (dispatch, getState) => {
      if (!getState().testReducer.isLoading) {
        fetch("https://jsonplaceholder.typicode.com/users").then(resp => resp.json())
        .then(list => {
            dispatch({ type: test.REQUEST_SUCCESS, list})
        })
      }
    };
  },
};

export default TestAction;