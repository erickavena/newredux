import { test } from "../../constanst/ActionTypes";

const initialState = {
  users: [],
  isLoading: false
};

const TestReducer = (values, action) => {
  const state = values || initialState;

  switch (action.type) {
    case test.REQUEST:
      return { ...state, isLoading: true};
    case test.REQUEST_SUCCESS:
      return { ...state, isLoading: false, users: action.list };
    default:
      return { ...state };
  }
};

export default TestReducer