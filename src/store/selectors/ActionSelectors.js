import TestAction from "../actions/TestAction";

// el ebjetivo de este archivo es agrupar los actions dependiendo la nececidad del componente
// se explica como un encapsulamiento
// TestAction Action Selector
export const TestAS = (dispatch) => ({
    getTestUsers: () => dispatch(TestAction.getTestUsers()),
});