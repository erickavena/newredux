// es lo mismo que en action selector pero para el estado de la aplicacion.

// Test action selector
export const TestSS = (useSelector) => ({
    isLoading: useSelector((state) => state.testReducer.isLoading),
    users: useSelector((state) => state.testReducer.users),
});
  